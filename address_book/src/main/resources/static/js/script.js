let onloadFlag = false;
window.onload = function() {
  getData();
  resetFields();
}

function resetFields(){
  document.getElementById("name").value = "";
  document.getElementById("phone").value = "";
  document.getElementById("company").value = "";
  document.getElementById("zip").value = "";
  document.getElementById("state").value = "";
}

function fillData(data){
  console.log(onloadFlag);
  let dataList = JSON.parse(data);
  dataList = dataList.filter(user=> user.registrationId == localStorage.getItem("id"));
  for (let index = 0; index < dataList.length; index++) {
    let eachCard = dataList[index];
    let newEmptyElement = `<div class="col-md-3 addressCard" id="addressCard">
    <div class="card home-card-styling">
      <div class="row">
        <div class="col-md-2">
          <div class="card-prof-pic">
            <i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
          </div>
        </div>
        <div class="col-md-7">
          <div class="card-body">
            <h4 class="card-title">`+dataList[index].personName+`</h4>
            <p class="card-text">`+dataList[index].address+`</p>
            <p class="card-text">`+dataList[index].city+`</p>
            <p class="card-text">`+dataList[index].state+`</p>
            <p class="card-text">`+dataList[index].zip+`</p>
          </div>
        </div>
        <div class="col-md-3">
          <button type="button" onclick="deleteAddress(`+dataList[index].personId+`, getData)" class="btn fa fa-trash"></button>
        </div>
        
      </div>
    </div>
  </div>`;
  if(document.getElementsByClassName("addressCard").length <= index){
    $("#cardsStack").append($.parseHTML(newEmptyElement));
  }
}
  console.log(document.getElementsByClassName("addressCard").length)+1;
  
}

function addNewCard(){
    /* appending a address card HTML element to the existing element */
    // console.log(document.getElementById('cardsStack').getElementsByClassName('addressCard')[0].append(sampleEmptyCard));
    let cardsStack = $("#cardsStack");
    let addressCards = cardsStack.find('div.addressCard');
    console.log(addressCards.length+" length");
    let lastCard = addressCards[addressCards.length-1];
    if(addressCards.length === 0){
        cardsStack.append(emptyCard());
    }else{
        let newEmptyCard = emptyCard(--addressCards.length);
        $("#cardsStack").append($.parseHTML(newEmptyCard));
    }
    
}

function emptyCard(Id){
    console.log(Id+1);
    Id++;
    let newEmptyElement = `<div class="col-md-3 addressCard" id="addressCard"`+Id+`>
    <div class="card home-card-styling">
      <div class="row">
        <div class="col-md-3">
          <div class="card-prof-pic">
            <i class="fa fa-user-circle fa-4x" aria-hidden="true"></i>
          </div>
        </div>
        <div class="col-md-7">
          <div class="card-body">
            <h4 class="card-title">Name</h4>
            <p class="card-text">phone</p>
          </div>
        </div>
        <div class="col-md-2">
          <button type="button" class="btn btn-primary">trash</button>
        </div>
      </div>
    </div>
  </div>`;
  console.log();
  return newEmptyElement;
}

function newAddress(){
  let name,phone,address;
  name = document.getElementById("name").value;
  city = document.getElementById("phone").value;
  address = document.getElementById("company").value;
  state = document.getElementById("state").value;
  zip = document.getElementById("zip").value;
  
  postData(name, city, address, state, zip);
  resetFields();
}

function postData(name, city, address, state, zip){
  let ENDPOINT_URL = "http://localhost:8080/addressbook/add/"+name+"/"+address+"/"+city+"/"+state+"/"+zip+"/"+localStorage.getItem("id");
  $.ajax({
    type: "POST",
    url: ENDPOINT_URL,
    dataType: 'json',
    success: function(result){
        console.log(result);
        location.reload();
    },  statusCode: {
      200: function() {
        location.reload();
      }
    }
  });
}

function updateData(){
  let name,phone,address;
  name = document.getElementById("updateName").value;
  phone = document.getElementById("updatePhone").value;
  company = document.getElementById("updateCompany").value;
  id = document.getElementById("updateId").value;
  let ENDPOINT_URL = "http://localhost:8080/updateAddress";
  $.ajax({
    type: "POST",
    url: ENDPOINT_URL + "?name=" + name + " &phone=" + phone+ "&company="+ company+"&id="+id,
    dataType: 'json',
    success: function(result){
        console.log(result);
    
    },  statusCode: {
      200: function() {
        location.reload();
      }
    }
  });0
  resetFields();
}



function getData(){
  console.log('Fetching Data');
  let ENDPOINT_URL = "http://localhost:8080/addressbook/getAddress";
  $.ajax({
    type: "GET",
    url: ENDPOINT_URL,
    success: function(data){
      console.log(data);
      fillData(data);
    }
  });
}

function deleteAddress(id){
  let ENDPOINT_URL = "http://localhost:8080/addressbook/delete/"+id;
  $.ajax({
    type: "GET",
    url: ENDPOINT_URL,
    dataType: 'json',
    success: function(result){
        console.log(result); 
        location.reload();
    },  statusCode: {
      200: function() {
        location.reload();
      }
    }
  });
  
}

function updateAddressUI(id, name, phone, company){
  document.getElementById("updateName").value = name;
  document.getElementById("updatePhone").value = phone;
  document.getElementById("updateId").value = id;
  document.getElementById("updateCompany").value = company;
  $("#updateModal").modal("toggle");
}
