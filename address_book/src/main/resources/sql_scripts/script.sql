show databases;
CREATE database address_book;
use address_book;
show tables;
CREATE TABLE IF NOT EXISTS t_person (
    person_id INT AUTO_INCREMENT PRIMARY KEY,
    person_name VARCHAR(255) NOT NULL,
    person_phone VARCHAR(255),
    person_company VARCHAR(255) NOT NULL,
    regisration_id INT
);
ALTER TABLE t_person
ADD COLUMN address VARCHAR(255) AFTER person_name,
ADD COLUMN city VARCHAR(255) AFTER person_name,
ADD COLUMN state VARCHAR(255) AFTER person_name,
ADD COLUMN zip VARCHAR(255) AFTER person_name;

ALTER TABLE t_person ADD CONSTRAINT fk_regesration_id FOREIGN KEY ( regesration_id ) REFERENCES t_users ( user_id ) ON DELETE CASCADE ON UPDATE RESTRICT;  

CREATE TABLE IF NOT EXISTS t_users (
    user_id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    first_name VARCHAR(255),
    last_name VARCHAR(255) NOT NULL,
    userpassword VARCHAR(255) NOT NULL
);


select * from t_person;
select * from t_users;