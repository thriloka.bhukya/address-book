package edu.missouristate.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.dao.AddressBookModel;
import edu.missouristate.dao.repository.AddressRepository;
import edu.missouristate.service.AddressBookService;

@Service
public class AddressBookServiceImplementation implements AddressBookService {

	@Autowired
	AddressRepository repository;

	@Override
	public AddressBookModel getAddress(int Id) {
		return repository.getOne(Id);
	}

	@Override
	public AddressBookModel addAddress(AddressBookModel address) {
		return repository.save(address);
	}

	@Override
	public List<AddressBookModel> getAllAddress() {
		return repository.findAll();
	}

	@Override
	public AddressBookModel updateAddress(AddressBookModel address) {
		return repository.save(address);
	}

	@Override
	public void deleteAddress(int id) {
		repository.deleteById(id);
	}

}
