package edu.missouristate.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.dao.Users;
import edu.missouristate.dao.repository.UserRepository;
import edu.missouristate.service.UserService;

@Service
public class UserServiceImplementation implements UserService {

	@Autowired
	UserRepository repository;

	@Override
	public Users getUser(String userName, String password) {
		return repository.findByUsernameAndUserpassword(userName, password);
	}

	@Override
	public Users addUser(Users user) {
		repository.save(user);
		return user;
	}

}
