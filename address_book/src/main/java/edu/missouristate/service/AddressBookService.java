package edu.missouristate.service;

import java.util.List;

import edu.missouristate.dao.AddressBookModel;

public interface AddressBookService {

	public AddressBookModel getAddress(int Id);

	public AddressBookModel addAddress(AddressBookModel address);

	public List<AddressBookModel> getAllAddress();

	public AddressBookModel updateAddress(AddressBookModel address);

	public void deleteAddress(int Id);

}
