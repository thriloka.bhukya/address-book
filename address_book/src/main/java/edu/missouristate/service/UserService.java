package edu.missouristate.service;

import edu.missouristate.dao.Users;

public interface UserService {

	public Users getUser(String username, String password);

	public Users addUser(Users user);

}
