package edu.missouristate.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.GsonBuilder;

import edu.missouristate.dao.HibernateProxyTypeAdapter;
import edu.missouristate.dao.Users;
import edu.missouristate.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService service;

	@CrossOrigin(origins = "*")
	@PostMapping("/{username}/{password}/{firstName}/{lastName}")
	public ResponseEntity<String> addUser(@PathVariable("username") String username,
			@PathVariable("password") String password, @PathVariable("firstName") String firstName,
			@PathVariable("lastName") String lastName) {
		service.addUser(new Users(0, username, firstName, lastName, password));
		return new ResponseEntity<String>("CREATED", HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/{username}/{password}")
	public ResponseEntity<String> getUserById(@PathVariable("username") String username,
			@PathVariable("password") String password) {

		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		Users user = service.getUser(username, password);
		if (user != null)
			return new ResponseEntity<String>(builder.create().toJson(user), HttpStatus.OK);
		else
			return new ResponseEntity<String>("User_not_found", HttpStatus.NOT_FOUND);
	}
}
