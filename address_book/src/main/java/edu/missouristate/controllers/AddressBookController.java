package edu.missouristate.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import edu.missouristate.dao.AddressBookModel;
import edu.missouristate.service.AddressBookService;

@RestController
@RequestMapping("/addressbook")
public class AddressBookController {

	@Autowired
	AddressBookService service;

	@CrossOrigin(origins = "*")
	@PostMapping("/add/{name}/{address}/{city}/{state}/{zip}/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<String> addAddress(@PathVariable("name") String name, @PathVariable("address") String address,
			@PathVariable("city") String city, @PathVariable("state") String state, @PathVariable("zip") String zip,
			@PathVariable("id") int id) {
		service.addAddress(new AddressBookModel(0, name, address, city, state, zip, name, address, id));
		return new ResponseEntity<String>("", HttpStatus.CREATED);
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/getAddress")
	public ResponseEntity<String> getAddress() {
		return new ResponseEntity<String>(new Gson().toJson(service.getAllAddress()), HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@GetMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") int id) {
		service.deleteAddress(id);
		return new ResponseEntity<String>(new Gson().toJson("Deleted"), HttpStatus.OK);
	}

}
