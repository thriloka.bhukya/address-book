package edu.missouristate.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_person")
public class AddressBookModel {

	@Override
	public String toString() {
		return "AddressBookModel [personId=" + personId + ", personName=" + personName + ", address=" + address
				+ ", city=" + city + ", state=" + state + ", zip=" + zip + ", personPhone=" + personPhone
				+ ", personCompany=" + personCompany + ", registrationId=" + registrationId + "]";
	}

	@Id
	@Column(name = "person_id")
	private int personId;
	@Column(name = "person_name")
	private String personName;
	@Column(name = "address")
	private String address;
	@Column(name = "city")
	private String city;
	@Column(name = "state")
	private String state;
	@Column(name = "zip")
	private String zip;
	@Column(name = "person_phone")
	private String personPhone;
	@Column(name = "person_company")
	private String personCompany;
	@Column(name = "regisration_id")
	private Integer registrationId;

	public Integer getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(Integer registrationId) {
		this.registrationId = registrationId;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPersonPhone() {
		return personPhone;
	}

	public void setPersonPhone(String personPhone) {
		this.personPhone = personPhone;
	}

	public String getPersonCompany() {
		return personCompany;
	}

	public void setPersonCompany(String personCompany) {
		this.personCompany = personCompany;
	}

	public AddressBookModel(int personId, String personName, String address, String city, String state, String zip,
			String personPhone, String personCompany, int registrationId) {
		super();
		this.personId = personId;
		this.personName = personName;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.personPhone = personPhone;
		this.personCompany = personCompany;
		this.registrationId = registrationId;
	}

	public AddressBookModel() {
	}

}
