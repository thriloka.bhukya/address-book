package edu.missouristate.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.missouristate.dao.AddressBookModel;

public interface AddressRepository extends JpaRepository<AddressBookModel, Integer> {

}
