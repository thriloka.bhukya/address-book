package edu.missouristate.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.missouristate.dao.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {

	public Users findByUsernameAndUserpassword(String username, String password);

}
